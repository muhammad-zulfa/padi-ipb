
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-push-2">
                <div class="card panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="img-container col-xs-3" style="background-image:url('<?php echo $this->config->item('layout_front') ?>img/img1.jpg')">
                            </div>
                            <div class="col-xs-9">
                                <h1>News Title</h1>
                                <p>posted by admin</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales ultrices neque, id euismod lectus dignissim sit amet. Donec dignissim enim quis enim congue, eu rutrum neque tincidunt. Ut sit amet pellentesque massa, et sollicitudin velit. Ut faucibus suscipit rutrum. Nunc eget pellentesque tellus.</p>
                                <div class="expander"><i class="fa fa-angle-down" data-toggle="collapse" data-target=".collapse"></i></div>
                                <div class="content-label">
                                    <p><a href="<?php echo base_url('home/news/view/id');?>">Read More...</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="img-container col-xs-3" style="background-image:url('<?php echo $this->config->item('layout_front') ?>img/img2.jpg')">
                            </div>
                            <div class="col-xs-9">
                                <h1>News Title</h1>
                                <p>posted by admin</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales ultrices neque, id euismod lectus dignissim sit amet. Donec dignissim enim quis enim congue, eu rutrum neque tincidunt. Ut sit amet pellentesque massa, et sollicitudin velit. Ut faucibus suscipit rutrum. Nunc eget pellentesque tellus.</p>
                                <div class="expander"><i class="fa fa-angle-down" data-toggle="collapse" data-target=".collapse"></i></div>
                                <div class="content-label">
                                    <p><a href="<?php echo base_url('home/news/view/id');?>">Read More...</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="img-container col-xs-3" style="background-image:url('<?php echo $this->config->item('layout_front') ?>img/img3.jpg')">
                            </div>
                            <div class="col-xs-9">
                                <h1>News Title</h1>
                                <p>posted by admin</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales ultrices neque, id euismod lectus dignissim sit amet. Donec dignissim enim quis enim congue, eu rutrum neque tincidunt. Ut sit amet pellentesque massa, et sollicitudin velit. Ut faucibus suscipit rutrum. Nunc eget pellentesque tellus.</p>
                                <div class="expander"><i class="fa fa-angle-down" data-toggle="collapse" data-target=".collapse"></i></div>
                                <div class="content-label">
                                    <p><a href="<?php echo base_url('home/news/view/id');?>">Read More...</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="img-container col-xs-3" style="background-image:url('<?php echo $this->config->item('layout_front') ?>img/img4.jpg')">
                            </div>
                            <div class="col-xs-9">
                                <h1>News Title</h1>
                                <p>posted by admin</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales ultrices neque, id euismod lectus dignissim sit amet. Donec dignissim enim quis enim congue, eu rutrum neque tincidunt. Ut sit amet pellentesque massa, et sollicitudin velit. Ut faucibus suscipit rutrum. Nunc eget pellentesque tellus.</p>
                                <div class="expander"><i class="fa fa-angle-down" data-toggle="collapse" data-target=".collapse"></i></div>
                                <div class="content-label">
                                    <p><a href="<?php echo base_url('home/news/view/id');?>">Read More...</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav>
                    <ul class="pagination center-block">
                        <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>